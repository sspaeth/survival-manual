Survival Manual
==
This is based on the US ARMY Survival handbook.
Original content prepared by Ligi, however, it is not clear what license their content is under, so the plan is to redo this from scratch from the 2018 revision of the book ([PDF](https://irp.fas.org/doddir/army/atp3-50-21.pdf)) and to also provide translations, internationalization for this.

Rather than making this an app, the plan is to distribute this in (local) html format and as an .epub, to be read with any book reader.

The latest version of this book can be read online [here](https://sspaeth.gitlab.io/survival-manual/).

License
===
The content of this repository is under the CC0 license.
